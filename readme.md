# [yalcin-development](https://bitbucket.org/javapda/yalcin-development)

## prerequisites
* java 8+ installed and available from PATH
* maven installed and available from PATH

## setup
```
git clone git@bitbucket.org:javapda/yalcin-development.git
cd yalcin-development
mvn package
```
## after setup
* open browser to file located: `..../yalcin-development/target/site/jacoco/index.html`

## other commands
* find help : list information about the jacoco plugin, including goals
```
mvn help:describe -Dplugin=org.jacoco:jacoco-maven-plugin -Ddetail
```

## Resources
* [jacoco home page](https://www.jacoco.org/jacoco/)
* [from mykong](https://mkyong.com/maven/maven-jacoco-code-coverage-example/)
* [jacoco-maven-plugin 0.8.7](https://mvnrepository.com/artifact/org.jacoco/jacoco-maven-plugin/0.8.7)
* [JaCoCo](https://www.jacoco.org/jacoco/trunk/doc/maven.html)

